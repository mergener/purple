#include <iostream>
#include <fstream>

#include "core/purdefs.h"
#include "vm/rtunit.h"
#include "vm/vm.h"

using namespace Purple;

#ifdef PURPLE_WINDOWS
	#include <Windows.h>
    static HWND hwnd;
    #define FATAL_RUNTIME_ERROR() \
        MessageBox ( \
            hwnd,\
            "Purple Runtime Error", \
            NULL, \
            MB_OK); \
        exit(1)
#else
    #define FATAL_RUNTIME_ERROR() \
        std::cerr << "Purple Framework v" << PURPLE_VERSION_MAJOR << "." \
        << PURPLE_VERSION_MEDIUM << "." << PURPLE_VERSION_MINOR << ": " \
        << "A fatal runtime error has occurred; this application must close immediately." \
        << std::endl; \
        exit(1)
#endif

static std::vector<std::string>& generate_args_array(int argc, char** argv)
{
	std::vector<std::string>& ret = *new std::vector<std::string>(argc);
	
	for (int i = 0; i < argc; i++)
	{
		ret.push_back(argv[i]);
	}

	return ret;
}

int main(int argc, char** argv)
{
	int ret;

	std::cout.sync_with_stdio(false);
	std::cin.sync_with_stdio(false);
	
    try 
    {
        if (argc < 2)
        {
            std::cerr << "No executable file found." << std::endl;
			system("pause");
            return 1;
        }

		// create the VM object
		VM vm(argv[1], generate_args_array(argc, argv));
		ret = vm.start();
    }
    catch (const std::exception& e)
    {
		std::cout << "Fatal exception occurred: " << e.what() << std::endl;
        FATAL_RUNTIME_ERROR();
    }

	system("pause");

    return ret;
}
