namespace purple.containers:

    with purple.meta;
	with purple.utils;
	with purple.algorithm;

    /*
        Standard Purple implementation for dynamic array. This generic data structure allows storage
        of elements in a contiguous, random-accessible way, while automatically handling reallocation
        of elements whenever it reaches its capacity.
    */
    ref class ArrayList<type T> 
        implements iContainer<T>, iRandomAccessible<T>:

    public:
        const int DEFAULT_CAPACITY = 5;
		alias ComparationFunction = function<int(T, T)>

		function add(T val):
			if count == array.getSize():
				resizeArray(array.getSize()*2);
			end

			array[count] = val;
			count++;
		end

		function remove(T val):
			removeAt(indexOf(val));
		end

		function removeAt(int index):
			count--;

			for int i = index; i < count; i++:
				array[index] = array[index + 1];
			end

			if count < array.getSize()/3:
				resize(array.getSize()/3);
			end
		end		

        readonly coroutine iContainer<T>.enumerate() -> T:
            for i = 0; i < count; i++:
                yield array[i];
            end
        end

        readonly function iRandomAccessible<T>.indexOf(T val) -> int:
            for int i = 0; i < array.getSize(); i++:
                if array[i] == val:
                    return i;
                end
            end
            
            return -1;
        end	

        readonly function iContainer<T>.contains(T val) -> indexOf(val) != -1;
        readonly function getCapacity() -> array.getSize();
        readonly function getCount() -> count;
        readonly function iRandomAccessible<T>.getWithIndex(int idx) -> array[idx];

        constructor():
            array = new T[DEFAULT_CAPACITY];
			count = 0;
        end

        constructor(int size):
            array = new T[size];
			count = 0;
        end

        iContainer<T>.constructor(readonly T[] args):
			array = new T[](args);
			count = args.getSize();
        end
		
        constructor(ArrayList<T> other):
            array = new T[other.array.getSize()];
			count = other.count;

			for int i = 0; i < count; i++:
				array[count] = other[count];
			end
        end

    private:
        T[] array;
        int count;

		function resizeArray(int newSize):
			T[] newArray = new T[newSize];
			int transferCount = min(newSize, count);

			for int i = 0; i < transferCount; i++:
				newArray[i] = array[i];	
			end
			
			array = newArray;
		end
    end
end