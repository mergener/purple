# project information
VERSION = 0.1
NAME = Purple

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

# compiler / flags
CXX = g++
CXXFLAGS = -Wall -Wextra -Wpedantic -Icore -Icompiler -Ivm
LDXXFLAGS =

# object files
OBJCORE    = core/purcode.o core/purlib.o core/purstring.o
OBJPURPLE  = purple.o vm/internal_calls.o vm/memory_manager.o vm/rtunit.o vm/vm.o vm/vmstack.o
OBJPURPLEC = purplec.o compiler/cmpobj.o compiler/ctunit.o compiler/functions.o compiler/namespace.o compiler/types.o

OBJTOTAL = $(OBJCORE) $(OBJPURPLE) $(OBJPURPLEC)

all: options purple #purplec

options:
	@echo "$(NAME) ($(VERSION)) build options"; \
	echo "	CXX         = $(CXX)"; \
	echo "	CXXFLAGS    = $(CXXFLAGS)"; \
	echo "	LXXDFLAGS   = $(LDXXFLAGS)"

purple: $(OBJCORE) $(OBJPURPLE)
	$(CXX) -o $@ $(OBJCORE) $(OBJPURPLE) $(LDXXFLAGS)

purplec: $(OBJCORE) $(OBJPURPLEC)
	$(CXX) -o $@ $(OBJCORE) $(OBJPURPLEC) $(LDXXFLAGS)
.c.o:
	$(CXX) $(CXXFLAGS) -c $<

$(OBJ): core.h

# main files
purple.o: core/purdefs.h vm/rtunit.h vm/vm.h
purplec.o: compiler/namespace.h compiler/types.h

# core
core/purcode.o: core/purcode.h
core/purlib.cpp: core/purlib.h
core/purstring.o: core/purstring.h

# compiler
compiler/cmpobj.o: compiler/cmpobj.h
compiler/ctunit.o: compiler/ctunit.h
compiler/functions.o: compiler/functions.h
compiler/namespace.o: compiler/namespace.h compiler/types.h
compiler/types.o: compiler/types.h core/purdefs.h

# vm
vm/internal_calls.o: vm/internal_calls.h
vm/memory_manager.o: vm/memory_manager.h
vm/rtunit.o: vm/rtunit.h
vm/vm.o: vm/vm.h vm/internal_calls.h
vm/vmstack.o: vm/vmstack.h

install: purple purplec
	mkdir -p $(DESTDIR)$(PREFIX)/bin $(DESTDIR)$(MANPREFIX)/man1; \
	cp -f purple purplec $(DESTDIR)$(PREFIX)/bin; \

uninstall:
	cd $(DESTDIR)$(PREFIX)/bin; \
	rm -f purple purplec

clean:
	rm -f $(OBJTOTAL) purple purplec $(NAME)-*.tar.gz

dist: clean
	set -- *; \
	mkdir $(NAME)-$(VERSION); \
	cp -Rf "$$@" $(NAME)-$(VERSION); \
	tar -czf $(NAME)-$(VERSION).tar.gz $(NAME)-$(VERSION); \
	rm -rf $(NAME)-$(VERSION)


.PHONY: all options install uninstall clean dist
