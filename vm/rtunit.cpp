/*
    "rtunit.cpp"

    Implementation of the RTUnit (Run-time unit) class.
*/

#include "rtunit.h"
#include <iostream>
#include <cstring>

namespace Purple {

#define WRITE_ON_VAR(var) stream.read(reinterpret_cast<char*>(&var), sizeof(var))

	RTUnit::RTUnit(const std::string& dir)
        : filedir(dir)
    {
        stream.open(filedir, std::istream::binary);
		
        WRITE_ON_VAR(purple_version_major);
        WRITE_ON_VAR(purple_version_medium);
        WRITE_ON_VAR(purple_version_minor);
		WRITE_ON_VAR(endianess);
		WRITE_ON_VAR(data_start);
		WRITE_ON_VAR(data_end);
        WRITE_ON_VAR(instruction_start);
        WRITE_ON_VAR(instruction_end);
    }

    opcode RTUnit::rewind_to_first_instruction()
    {
        opcode ret;
        stream.seekg(instruction_start);
        WRITE_ON_VAR(ret);
        return ret;
    }

    void RTUnit::jump_to(qword pos)
    {
        auto dest = pos + instruction_start - 1;
        if (dest > instruction_end)
        {
            throw new std::exception();
        }

        stream.seekg(dest);
    }

    void RTUnit::jump_offset(qword offset)
    {
        auto dest = static_cast<qword>(stream.tellg()) + offset;
        if (dest > instruction_end)
        {
            throw new std::exception();
        }

        stream.seekg(dest);
    }

    void RTUnit::get_data_from_offset (
        qword offset,
        size_t data_amount,
        ByteBlock& outByteBlock)
    {
        auto oldpoint = stream.tellg();

        if (outByteBlock.block != nullptr)
        {
            delete[] outByteBlock.block;
        }
        
        stream.seekg(data_start + offset);

        outByteBlock.size = data_amount;
        outByteBlock.block = new byte[data_amount];

        stream.read(reinterpret_cast<char*>(outByteBlock.block), data_amount);

        stream.seekg(oldpoint);
    }

    opcode RTUnit::get_next_instruction(ByteBlock& outByteBlock)
    {
        if (stream.tellg() >= instruction_end)
        {
            return opcode::_last;
        }

        opcode op;
        WRITE_ON_VAR(op);

        int mem_amount = get_opcode_param_size(op);

		if (outByteBlock.block == nullptr)
		{
			outByteBlock.block = new byte[mem_amount];
			outByteBlock.size = mem_amount;
		}
		else if (outByteBlock.size < mem_amount)
		{
			delete[] outByteBlock.block;
			outByteBlock.block = new byte[mem_amount];
			outByteBlock.size = mem_amount;
		}

        stream.read(reinterpret_cast<char*>((outByteBlock.block)), mem_amount);

        return op;
    }

#undef WRITE_ON_VAR

} // Purple