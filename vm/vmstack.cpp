#include <cstring>
#include <cstdlib>
#include "vmstack.h"

namespace Purple
{
	static const int default_stack_size = 2048;

	VMStack::VMStack()
	{
		base_ptr = 0;
		top_ptr = 0;

		memory = new byte[default_stack_size];
		capacity = default_stack_size;
	}

	void VMStack::resize(p_size_t newSize)
	{
		byte* new_mem = new byte[newSize];

		std::memcpy(new_mem, memory, capacity);
		delete[] memory;
		capacity = newSize;
		memory = new_mem;
	}

	void VMStack::push(byte* block, p_size_t size)
	{
		top_ptr += size;

		if (top_ptr > capacity)
		{
			resize(capacity * 2);
		}

		std::memcpy(&memory[top_ptr], block, size);
	}

	void VMStack::push(ByteBlock& block)
	{
		push(block.block, block.size);
	}

	void VMStack::push(ByteBlock* block)
	{
		push(block->block, block->size);
	}

	byte* VMStack::pop(p_size_t amount)
	{
		byte* ret = new byte[amount];
		pop(amount, ret);
		return ret;
	}

	/*
		Important: expects the external_address pointer to be pre-allocated.
	*/
	void VMStack::pop(p_size_t amount, byte* external_address)
	{
		top_ptr -= amount;

		if (top_ptr < capacity/3)
		{
			resize(capacity / 3);
		}

		std::memcpy(external_address, memory, amount);
	}

	void VMStack::raise_stack()
	{
		base_ptr_history.push(base_ptr);
		base_ptr = top_ptr;
	}

	void VMStack::lower_stack()
	{
		top_ptr = base_ptr;
		base_ptr = base_ptr_history.top();
		base_ptr_history.pop();
	}

} // Purple
