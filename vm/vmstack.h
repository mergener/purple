#ifndef VM_STACk_H
#define VM_STACk_H

#include <stack>
#include <vector>

#include "../core/purdefs.h"

namespace Purple
{
    class VMStack {
    public:
        void push(byte* block, p_size_t size);
        void push(ByteBlock& block);
        void push(ByteBlock* block);

		inline p_size_t get_base_ptr() const { return base_ptr; }
		inline p_size_t get_top_ptr() const { return top_ptr; }
		void raise_stack();
		void lower_stack();

        byte* pop(p_size_t amount);
        void pop(p_size_t amount, byte* external_address);
        VMStack();

    private:
		void resize(p_size_t newSize);

        std::stack<p_size_t> base_ptr_history;
		byte* memory;

		p_size_t capacity;
		p_size_t base_ptr;
		p_size_t top_ptr;
    };
} // Purple


#endif // VM_STACk_H