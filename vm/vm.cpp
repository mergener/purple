#include <cstring>

#include "vm.h"
#include "internal_calls.h"

namespace Purple
{
	VM::VM(const std::string& rtpath, const std::vector<std::string>& cl_args)
		: 
		rtunit(rtpath),
		cl_args(cl_args),
		params(nullptr, 0)
    {
		for (int i = 0; i < static_cast<int>(buffer_address::_last); i++)
		{
			buffer_contents[i].qword_part = 0;
		}
    }

    int VM::start()
    {
        opcode op = rtunit.get_next_instruction(params);

        /*
            Read all const strings and make string objects.
        */

        while (op != opcode::_last)
        {
			std::cout << buffer_contents[0].qword_part << '\n';
            run_cmd(op);
            op = rtunit.get_next_instruction(params);
        }

        return 0;
    }

    inline void VM::run_cmd(opcode op)
    {
		/*
			Whenever a new command is about to be executed, the 'params' byteblock is filled with parameter information
			and its size will match the desired instruction parameter size as well.

			Remember that since 'params.block' is a BYTE array, every 1 point offset in the [] operator is a 1 byte offset.
		*/

        switch (op)
		{
			case opcode::purcall:
			{
				auto procedure_id = *reinterpret_cast<purcall_t*>(params.block);
				purcall(procedure_id, memory_manager.get_stack(), buffer_contents[0], memory_manager);
				break;
			}

			case opcode::jumpto:
			{
				auto destination = *reinterpret_cast<qword*>(params.block);
				rtunit.jump_to(destination);
				break;
			}

			case opcode::je:
			{
				auto bufferA = params.block[0];
				auto bufferB = params.block[1];
				if (buffer_contents[bufferA].qword_part == buffer_contents[bufferB].qword_part)
				{
					rtunit.jump_to(*reinterpret_cast<qword*>(&params.block[2]));
				}
				break;
			}

			case opcode::jg:
			{
				auto bufferA = params.block[0];
				auto bufferB = params.block[1];
				if (buffer_contents[bufferA].qword_part > buffer_contents[bufferB].qword_part)
				{
					rtunit.jump_to(*reinterpret_cast<qword*>(&params.block[2]));
				}
				break;
			}

			case opcode::jge:
			{
				auto bufferA = params.block[0];
				auto bufferB = params.block[1];
				if (buffer_contents[bufferA].qword_part >= buffer_contents[bufferB].qword_part)
				{
					rtunit.jump_to(*reinterpret_cast<qword*>(&params.block[2]));
				}
				break;
			}

			case opcode::jl:
			{
				auto bufferA = params.block[0];
				auto bufferB = params.block[1];
				if (buffer_contents[bufferA].qword_part < buffer_contents[bufferB].qword_part)
				{
					rtunit.jump_to(*reinterpret_cast<qword*>(&params.block[2]));
				}
				break;
			}

			case opcode::jle:
			{
				auto bufferA = params.block[0];
				auto bufferB = params.block[1];
				if (buffer_contents[bufferA].qword_part <= buffer_contents[bufferB].qword_part)
				{
					rtunit.jump_to(*reinterpret_cast<qword*>(&params.block[2]));
				}
				break;
			}

			case opcode::jne:
			{
				auto bufferA = params.block[0];
				auto bufferB = params.block[1];
				if (buffer_contents[bufferA].qword_part != buffer_contents[bufferB].qword_part)
				{
					rtunit.jump_to(*reinterpret_cast<qword*>(&params.block[2]));
				}
				break;
			}

			case opcode::emplq:
			{
				auto buffer = params.block[0];
				buffer_contents[buffer].qword_part = *reinterpret_cast<qword*>(&params.block[1]);
				break;
			}	

			case opcode::empld:
			{
				auto buffer = params.block[0];
				buffer_contents[buffer].qword_part = 0;
				buffer_contents[buffer].dword_part = *reinterpret_cast<dword*>(&params.block[1]);
				break;
			}	

			case opcode::emplw:
			{
				auto buffer = params.block[0];
				buffer_contents[buffer].qword_part = 0;
				buffer_contents[buffer].word_part = *reinterpret_cast<word*>(&params.block[1]);
				break;
			}	

			case opcode::emplb:
			{
				auto buffer = params.block[0];
				buffer_contents[buffer].qword_part = 0;
				buffer_contents[buffer].byte_part = params.block[1];
				break;
			}		

			case opcode::pop:
			{
				auto buffer = params.block[0];
				auto size = params.block[1];

				auto block = memory_manager.get_stack().pop(size);
				memory_manager.get_stack().pop(static_cast<p_size_t>(size), reinterpret_cast<byte*>(&buffer_contents[buffer]));
				break;
			}

			case opcode::push:
			{
				auto buffer = params.block[0];
				auto size = params.block[1];
				byte* block = new byte[size]; // (!) possible memory allocation errors are still not being handled, must revisit this code ASAP!
				std::memcpy(block, &buffer_contents[buffer], size);
				memory_manager.get_stack().push(block, size);
				break;
			}

			case opcode::replace:
			{
				auto src = params.block[0];
				auto dest = params.block[1];

				qword swap = buffer_contents[src].qword_part;
				buffer_contents[src].qword_part = buffer_contents[dest].qword_part;
				buffer_contents[dest].qword_part = swap;
				break;
			}

			case opcode::heapalloc:
			{
				auto buffer = params.block[0];
				dword size;
				std::memcpy(&size, &params.block[1], sizeof(dword));

				auto object = memory_manager.make_object(size); // (!) possible memory allocation errors are still not being handled, must revisit this code ASAP!
				break;
			}

			case opcode::call:
			{
				auto buffer = params.block[0];
				memory_manager.get_stack().raise_stack();
				call_stack.push(buffer_contents[buffer].qword_part);
				rtunit.jump_to(buffer_contents[buffer].qword_part);
				break;
			}

			case opcode::ret:
			{
				rtunit.jump_to(call_stack.top());
				call_stack.pop();
				break;
			}

			case opcode::addref:
			{
				auto buffer = params.block[0];
				auto object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				object->add_ref();
				break;
			}

			case opcode::subref:
			{
				auto buffer = params.block[0];
				auto object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				object->remove_ref();
				break;
			}

			case opcode::move:
			{
				auto dest = params.block[0];
				auto other = params.block[1];

				buffer_contents[dest].qword_part = buffer_contents[other].qword_part;
				break;
			}

			case opcode::sum:
			{
				auto dest = params.block[0];
				auto other = params.block[1];

				buffer_contents[dest].qword_part += buffer_contents[other].qword_part;
				break;
			}
				
			case opcode::sub:
			{
				auto dest = params.block[0];
				auto other = params.block[1];

				buffer_contents[dest].qword_part -= buffer_contents[other].qword_part;
				break;
			}

			case opcode::mul:
			{
				auto dest = params.block[0];
				auto other = params.block[1];

				buffer_contents[dest].qword_part *= buffer_contents[other].qword_part;
				break;
			}

			case opcode::div:
			{
				auto dest = params.block[0];
				auto other = params.block[1];
				
				buffer_contents[dest].qword_part /= buffer_contents[other].qword_part;
				break;
			}

			case opcode::sdiv:
			{
				auto dest = params.block[0];
				auto other = params.block[0];

				buffer_contents[dest].qword_part = static_cast<qword>(static_cast<sqword>(buffer_contents[dest].qword_part)/static_cast<sqword>(buffer_contents[other].qword_part));
				break;
			}

			case opcode::shl:
			{
				auto buffer = params.block[0];
				auto amount = params.block[1];

				buffer_contents[buffer].qword_part <<= amount;
				break;
			}

			case opcode::shr:
			{
				auto buffer = params.block[0];
				auto amount = params.block[1];
				
				buffer_contents[buffer].qword_part >>= amount;
				break;
			}

			case opcode::exit:
			{
				system("pause");
				std::exit(buffer_contents[static_cast<int>(buffer_address::bufA)].dword_part);
				break;
			}

			case opcode::heapwriteb:
			{
				auto ptr_buffer = params.block[0];
				auto content_buffer = params.block[1];
				auto offset = *reinterpret_cast<dword*>(&params.block[2]);

				byte* mem = reinterpret_cast<PurObject*>(buffer_contents[ptr_buffer].qword_part)->get_bytes() + offset;
				*mem = buffer_contents[content_buffer].byte_part;

				break;
			}

			case opcode::heapwritew:
			{
				auto ptr_buffer = params.block[0];
				auto content_buffer = params.block[1];
				auto offset = *reinterpret_cast<dword*>(&params.block[2]);

				word* mem = reinterpret_cast<word*>(reinterpret_cast<PurObject*>(buffer_contents[ptr_buffer].qword_part)->get_bytes() + offset);
				*mem = buffer_contents[content_buffer].word_part;

				break;
			}

			case opcode::heapwrited:
			{
				auto ptr_buffer = params.block[0];
				auto content_buffer = params.block[1];
				auto offset = *reinterpret_cast<dword*>(&params.block[2]);

				dword* mem = reinterpret_cast<dword*>(reinterpret_cast<PurObject*>(buffer_contents[ptr_buffer].qword_part)->get_bytes() + offset);
				*mem = buffer_contents[content_buffer].dword_part;

				break;
			}

			case opcode::heapwriteq:
			{
				auto ptr_buffer = params.block[0];
				auto content_buffer = params.block[1];
				auto offset = *reinterpret_cast<dword*>(&params.block[2]);

				qword* mem = reinterpret_cast<qword*>(reinterpret_cast<PurObject*>(buffer_contents[ptr_buffer].qword_part)->get_bytes() + offset);
				*mem = buffer_contents[content_buffer].qword_part;

				break;
			}

			case opcode::heapcpy:
			{
				auto dest_ptr = reinterpret_cast<byte*>(buffer_contents[params.block[0]].qword_part);
				auto src_ptr = reinterpret_cast<byte*>(buffer_contents[params.block[1]].qword_part);
				auto size = *reinterpret_cast<dword*>(&params.block[2]);
				std::memcpy(dest_ptr, src_ptr, size);

				break;
			}

			case opcode::heapcpy_vs:
			{
				auto dest_ptr = reinterpret_cast<byte*>(buffer_contents[params.block[0]].qword_part);
				auto src_ptr = reinterpret_cast<byte*>(buffer_contents[params.block[1]].qword_part);
				auto size = buffer_contents[params.block[2]].qword_part;
				std::memcpy(dest_ptr, src_ptr, size);

				break;
			}

			case opcode::derefb:
			{
				// since heap allocated objects are always wrapped around a PurObject*, deref maps to the object pointed by
				// the PurObject*.
				auto buffer = params.block[0];
				auto offset = *reinterpret_cast<dword*>(&params.block[1]);

				// the pointer stored at the buffer points to the PurObject handle. Taking this in consideration, we must make
				// a double dereference.
				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				buffer_contents[buffer].qword_part = 0;
				std::memcpy(&buffer_contents[buffer].byte_part, object->get_bytes() + offset, sizeof(byte));
				break;
			}

			case opcode::derefw:
			{
				auto buffer = params.block[0];
				auto offset = *reinterpret_cast<dword*>(&params.block[1]);

				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				buffer_contents[buffer].qword_part = 0;
				std::memcpy(&buffer_contents[buffer].word_part, object->get_bytes() + offset, sizeof(word));
				break;
			}

			case opcode::derefd:
			{
				auto buffer = params.block[0];
				auto offset = *reinterpret_cast<dword*>(&params.block[1]);

				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				buffer_contents[buffer].qword_part = 0;
				std::memcpy(&buffer_contents[buffer].dword_part, object->get_bytes() + offset, sizeof(dword));
				break;
			}

			case opcode::derefq:
			{
				auto buffer = params.block[0];
				auto offset = *reinterpret_cast<dword*>(&params.block[1]);

				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				std::memcpy(&buffer_contents[buffer].qword_part, object->get_bytes() + offset, sizeof(qword));
				break;
			}

			case opcode::derefb_vo:
			{
				auto buffer = params.block[0];
				auto offset = buffer_contents[params.block[1]].qword_part;

				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				buffer_contents[buffer].qword_part = 0;
				std::memcpy(&buffer_contents[buffer].byte_part, object->get_bytes() + offset, sizeof(byte));
				break;
			}

			case opcode::derefw_vo:
			{
				auto buffer = params.block[0];
				auto offset = buffer_contents[params.block[1]].qword_part;

				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				buffer_contents[buffer].qword_part = 0;
				std::memcpy(&buffer_contents[buffer].word_part, object->get_bytes() + offset, sizeof(word));
				break;
			}

			case opcode::derefd_vo:
			{
				auto buffer = params.block[0];
				auto offset = buffer_contents[params.block[1]].qword_part;

				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				buffer_contents[buffer].qword_part = 0;
				std::memcpy(&buffer_contents[buffer].dword_part, object->get_bytes() + offset, sizeof(dword));
				break;
			}

			case opcode::derefq_vo:
			{
				auto buffer = params.block[0];
				auto offset = buffer_contents[params.block[1]].qword_part;

				PurObject* object = reinterpret_cast<PurObject*>(buffer_contents[buffer].qword_part);
				std::memcpy(&buffer_contents[buffer].qword_part, object->get_bytes() + offset, sizeof(qword));
				break;
			}

            default:
			{
				std::exit(1);
				break;
			}
        }
    }

} // Purple
