#ifndef PURPLE_VM_H
#define PURPLE_VM_H

#include <cstdint>
#include <array>
#include <memory>
#include <vector>
#include <stack>
#include <iostream>

#include "../core/purcode.h"
#include "../core/purdefs.h"
#include "rtunit.h"
#include "memory_manager.h"

namespace Purple {
	union buffer_content_t
	{
		byte byte_part;
		word word_part;
		dword dword_part;
		qword qword_part;
	};
    
    class VM {
    public:
        VM(const std::string& rtpath, const std::vector<std::string>& cl_args);
        
        int start();
        void run_cmd(opcode op);

    private:
        RTUnit rtunit;
        MemoryManager memory_manager;

        ByteBlock params;
		
		buffer_content_t buffer_contents[static_cast<int>(buffer_address::_last)];

		const std::vector<std::string> cl_args;
		std::stack<qword> call_stack;

        VM();
    };
} // Purple

#endif // PURPLE_VM_H