#include "memory_manager.h"
#include <cstdlib>

#ifdef USE_C_ALLOCATOR
    #define ALLOCATE(BYTES) reinterpret_cast<byte*>(malloc(BYTES))
    #define DEALLOCATE(PTR) free(PTR)
	#define DEALLOCATE_ARRAY(PTR) free(PTR)
#else
    #define ALLOCATE(BYTES) new byte[BYTES]
    #define DEALLOCATE(PTR) delete PTR
	#define DEALLOCATE_ARRAY(PTR) delete[] PTR
#endif

namespace Purple {
    void PurObject::remove_ref()
    {
        ref_count--;
        if (ref_count <= 0)
        {
            DEALLOCATE_ARRAY(this->ptr);
            DEALLOCATE(this);
        }
    }

    PurObject::PurObject(p_size_t size)
    {
        ptr = ALLOCATE(size);
    }

    PurObject* MemoryManager::make_object(p_size_t size)
    {
        return new PurObject(size);
    }

    void MemoryManager::free_object(PurObject* ptr)
    {
        DEALLOCATE_ARRAY(ptr);
    }
} // Purple
