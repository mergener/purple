#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include <memory>
#include <vector>
#include <functional>

#include "../core/purdefs.h"
#include "vmstack.h"

namespace Purple {

	/*
		Defines classes related to Purple's memory management.

		All heap memory allocated above C++ level must be wrapped around a PurObject, which allows reference
		counting.
	*/

    class PurObject {
    public:
		/* Counts one reference. */
        inline void add_ref() { ref_count++; }

		/* Reduces the reference count by 1. If it reaches zero and the object is not
		immune, it will be collected by the GC. */
        void remove_ref();

		/* Returns the pointer to this object's data block. */
		inline byte* get_bytes() const { return ptr; }

		/* Immune objects are not collected by the GC. */
        inline bool is_immune() const { return immune; }

		/* Immune objects are not collected by the GC. */
        void set_immune(bool b);

        inline byte operator*() const { return *ptr; }
        inline byte operator[](int i) const { return ptr[i]; }

    private:
        friend class MemoryManager;

        bool immune = false;
        byte* ptr = nullptr;
        int ref_count = 1;

        PurObject(p_size_t size);
    };

    class MemoryManager {
    public:
		/* Allocates the specified amount of heap memory and returns a pointer to the
		PurObject* that wraps the allocated block. */
        PurObject* make_object(p_size_t size);

		/* Explicitly frees the memory allocated by the specified object. */
        void free_object(PurObject* ptr);

		/* Returns this MemoryManager's stack. */
		inline VMStack& get_stack() { return stack; }

    private:
        VMStack stack;
    };
} // Purple

#endif // MEMORY_MANAGER_H