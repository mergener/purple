#include "internal_calls.h"

#include <cmath>

namespace Purple
{
	typedef std::function<void(VMStack&, buffer_content_t&, MemoryManager&)> purcall_function;

	static void purple_math_sin(VMStack& stack, buffer_content_t& ret_buf, MemoryManager& mem_manager)
	{
		double value = *reinterpret_cast<double*>(stack.pop(sizeof(double)));
		ret_buf.qword_part = *reinterpret_cast<qword*>(&sin(value));
	}
	
	static void purple_math_cosin(VMStack& stack, buffer_content_t& ret_buf, MemoryManager& mem_manager)
	{
		double value = *reinterpret_cast<double*>(stack.pop(sizeof(double)));
		ret_buf.qword_part = *reinterpret_cast<qword*>(&cos(value));		
	}
	
	static void purple_out_println(VMStack& stack, buffer_content_t& ret_buf, MemoryManager& mem_manager)
	{
		const char* str_to_print = reinterpret_cast<char*>(stack.pop(sizeof(str_to_print)));
		std::cout << str_to_print << '\n';
	}
	
	static void purple_io_readln(VMStack& stack, buffer_content_t& ret_buf, MemoryManager& mem_manager)
	{
		size_t current_buffer_size = 32;
		char* buffer = nullptr;
		char* newBuffer;
		
		// read from input stream
		while (!cin.eof())
		{
			if (buffer == nullptr)
			{
				newBuffer = new char[current_buffer_size * 2];
				std::memcpy(newBuffer, buffer, current_buffer_size);
				
				delete[] buffer;
				buffer = newBuffer;
				cin.read(current_buffer_size);
				current_buffer_size *= 2;
			}
			else
			{
				buffer = new char[current_buffer_size];
				cin.read(current_buffer_size);
			}
			
		}
		
		// return the result 
		ret_buf.qword_part = mem_manager.make_object(current_buffer_size);
	}

	//
	// Important!
	//
	//		- The following array must *always* follow the same order as the
	//		purcall_t enum class (defined at internal_calls.h). If this rule is broken,
	//		severe undefined behaviour will happen.
	//
	
	static purcall_function purcall_functions_list[] =
	{
		purple_out_println,
		purple_io_readln,
		purple_math_sin,
		purple_math_cosin
	};

	void purcall(purcall_t id, VMStack& stack, buffer_content_t& ret_buf, MemoryManager& mem_manager)
	{
		purcall_functions_list[static_cast<int>(id)](stack, ret_buf, mem_manager);
	}
} // Purple
