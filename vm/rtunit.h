/*
    "rtunit.h"

    Interface for the RTunit (Run-time unit) class.
*/

#ifndef RTUNIT_H
#define RTUNIT_H

#include <fstream>
#include <string>
#include <stack>
#include <vector>
#include <sstream>
#include "../core/purdefs.h"
#include "../core/purcode.h"

namespace Purple {

    /*
        Class for handling Run-time units.

        Run-time units are wrappers and readers for compiled purple bytecode files.
    */
    class RTUnit {
    public:
        RTUnit(const std::string& dir);

        inline ~RTUnit()
        {
            stream.close();
        }

        /* Returns the next instruction and fills the ByteBlock with parameter info. */
        opcode get_next_instruction(ByteBlock& outByteBlock);
        
        /* Moves the file pointer to the beginning of the instruction section and returns
        the first opcode. */
        opcode rewind_to_first_instruction();

		/* Moves the instruction pointer <offset> bytes ahead. */
        void jump_offset(qword offset);

		/* Moves the instruction pointer to the specified position. */
        void jump_to(qword pos);

        /* Gets <data_amount> bytes of data at <offset> bytes from beginning of data section
        and fills the <outByteBlock> with the requested data. */
        void get_data_from_offset (
            qword offset,
            size_t data_amount,
            ByteBlock& outByteBlock
        );

        inline byte get_version_major() const { return purple_version_major; }
        inline byte get_version_medium() const { return purple_version_medium; }
        inline byte get_version_minor() const { return purple_version_minor; }
        inline qword get_instruction_start() const { return instruction_start; }
        inline qword get_instruction_end() const { return instruction_end; }
        inline qword get_data_start() const { return data_start; }
        inline qword get_data_end() const { return data_end; }

    private:
        std::ifstream stream;
        const std::string& filedir;

        byte purple_version_major;
        byte purple_version_medium;
        byte purple_version_minor;
		endian endianess;

		std::stringstream instruction_buffer_stream;
		std::string instruction_buffer;
		qword filepos;

        qword instruction_start;
        qword instruction_end;
        qword data_start;
        qword data_end;
        qword const_str_start;
        qword const_str_end;

        std::stack<qword> prev_indexes;

        RTUnit();
    };

    } // Purple


#endif // RTUNIT_H