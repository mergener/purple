#ifndef PURPLE_INTERNAL_CALLS_H
#define PURPLE_INTERNAL_CALLS_H

#include "../core/purdefs.h"
#include "vmstack.h"
#include "vm.h"

namespace Purple {
	enum class purcall_t : dword
	{
		purple_out_println = 0,
		purple_io_readln   = 1,
		purple_math_sin	   = 2,
		purple_math_cosin  = 3
	};

	void purcall(purcall_t id, VMStack& stack, buffer_content_t& ret_buf);
} // Purple


#endif // PURPLE_INTERNAL_CALLS_H