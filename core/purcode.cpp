#include "purcode.h"
#include "../vm/internal_calls.h"

namespace Purple {
    int get_opcode_param_size(opcode op)
    {
        switch (op)
        {
			case opcode::je:
			case opcode::jne:
			case opcode::jg:
			case opcode::jge:
			case opcode::jl:
			case opcode::jle:
				return 10;

            case opcode::emplq:
                return 9;

			case opcode::jumpto:
				return 8;

			case opcode::heapcpy:
			case opcode::heapwriteb:
			case opcode::heapwritew:
			case opcode::heapwrited:
			case opcode::heapwriteq:
				return 6;

			case opcode::heapalloc:
            case opcode::empld:
			case opcode::derefb:
			case opcode::derefw:
			case opcode::derefd:
			case opcode::derefq:
				return 5;

            case opcode::purcall:
                return sizeof(purcall_t);

			case opcode::heapcpy_vs:
			case opcode::heapwriteb_vo:
			case opcode::heapwritew_vo:
			case opcode::heapwrited_vo:
			case opcode::heapwriteq_vo:
            case opcode::emplw:
                return 3;

			case opcode::push:
			case opcode::pop:
			case opcode::sum:
			case opcode::sub:
			case opcode::mul:
			case opcode::div:
			case opcode::smul:
			case opcode::sdiv:
			case opcode::move:
			case opcode::shl:
			case opcode::shr:
			case opcode::_xor:
            case opcode::replace:
            case opcode::emplb:
			case opcode::derefb_vo:
			case opcode::derefw_vo:
			case opcode::derefd_vo:
			case opcode::derefq_vo:
				return 2;

			case opcode::subref:
			case opcode::call:
            case opcode::syscall:
			case opcode::xcall:
			case opcode::addref:
				return 1;

            case opcode::ret:
            case opcode::exit:
                return 0;

            case opcode::_last:
				std::cerr << "Illegal instruction, program aborted.\n";
                exit(1);

            default:
                return 2;      
        }

        return 0;
    }

} // Purple
