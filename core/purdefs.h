
/*
    "purdefs.h"
 
    Contains important definitions for Purple.
*/

#ifndef PURDEFS_H
#define PURDEFS_H

#include <string>
#include <cstdint>

//
// Version info:
// (current version: 1.0.0)
//

#define PURPLE_VERSION_MAJOR  1
#define PURPLE_VERSION_MEDIUM 0
#define PURPLE_VERSION_MINOR  0

#define PURPLE_VERSION PURPLE_VERSION_MAJOR * 100 + PURPLE_VERSION_MEDIUM * 10 + PURPLE_VERSION_MINOR

//
// Architecture check:
//

#if defined(__X86_X64) || defined(__X86_X64__) || defined(__amd64) || defined (__amd64__) || defined(_M_AMD64) || defined (_M_X64)
    #define PURPLE_X64
#elif defined(i386) || defined(__i386) || defined(__i386__) || defined(_M_IX86) || defined(_X86_)
    #define PURPLE_X86
#else
    #error "This machine's architecture is not supported."
#endif

#if defined(i386) || defined(__i386) || defined(__i386__)
    #define PURPLE_I386
#elif defined(__amd64) || defined(__amd64__) || defined(_M_AMD64)
    #define PURPLE_AMD64
#endif

//
// Platform check:
//

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    #define PURPLE_WINDOWS
#elif defined(linux)
    #define PURPLE_LINUX
#endif

#if defined(__unix__)
    #define PURPLE_UNIX
#endif

//
// Annotations:
// 

/*
	Declare classes as interfaces when they provide no fields and only
	pure virtual (abstract) methods.
*/
#define interface class
;
#define abstract

namespace Purple {

    //
    // General primitives:
    //

    using byte = uint8_t;
    using word = uint16_t;
    using dword = uint32_t;
    using qword = uint64_t;
    using sbyte = int8_t;
    using sword = int16_t;
    using sdword = int32_t;
    using sqword = int64_t;

    //
    // Purple primitives:
    //

    using purbool = sbyte;
    using purint = sdword;
    using purlong = sqword;
    using puruint = dword;
    using purulong = qword;
    using purfloat = float;
    using purdouble = double;
    using purshort = sword;
    using purchar = sword;
    using purbyte = byte;
    using pursbyte = sbyte;
    class PurString; 
    using purstring = PurString; 

    #if defined(PURPLE_x86)
        using p_size_t = uint32_t;
    #elif defined(PURPLE_X64)
        using p_size_t = uint64_t;
    #else
        using p_size_t = size_t;
    #endif

	enum class endian : byte
	{
		big,
		little
	};

    struct ByteBlock
    {
        byte* block = nullptr;
        size_t size = 0;

		inline ByteBlock(byte* block, size_t size)
			: block(block), size(size) { }

	private:
		ByteBlock();
    };

} // Purple

#endif // PURDEFS_H