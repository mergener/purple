#ifndef PURSTRING_H
#define PURSTRING_H

#include <memory>
#include <string>
#include <unordered_map>

namespace Purple
{
    class PurString
    {
		friend PurString& purple_make_str(const std::string& str);

    public:
		inline size_t size() const { return str.size(); }
		inline char operator[](size_t i) const { return str[i]; }

        PurString& operator+(const PurString& other) const;

    private:
        std::string str;
		static std::unordered_map<std::string, PurString*> pstrings;

        PurString(std::string s);
    };


} // Purple


#endif // PURSTRING_H