#include <string>
#include <vector>

#include "purdefs.h"

namespace Purple {
	/*
		An ExportedFunction is a function that is exposed within a PLB binary.
	*/
	class ExportedFunction {
	private:
		PurpleLibrary* library;
		std::string name;
		qword entry_point;
		qword exit_point;
	};

	class PurpleLibrary {
	public:

	private:
		std::vector<ExportedFunction> exported_functions;
	};
}