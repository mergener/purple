#include "purstring.h"
#include <stdexcept>

namespace Purple
{
    PurString& purple_make_str(const std::string& str)
    {
        PurString* retptr;
        try
        {
            retptr = PurString::pstrings.at(str);
        }
        catch (std::out_of_range)
        {
            retptr = new PurString(str);
        }

        return *retptr;
    }

	std::unordered_map<std::string, PurString*> PurString::pstrings;

    PurString::PurString(std::string s)
        : str(s) {}

    PurString& PurString::operator+(const PurString& other) const
    {
        return purple_make_str((str + other.str).c_str());
    }
} // Purple
