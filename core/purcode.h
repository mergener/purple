#ifndef PURCODE_H
#define PURCODE_H

#include "purdefs.h"

#ifdef SAVE_BUFFER_ADDRESS_AND_OPS_AS_STR
    #include <string>
    #include <vector>
#endif

namespace Purple {
    enum class opcode : word
    {
        heapalloc,
        addref,
        subref,
        derefb,
        push,
        pop,
		move,
		jumpto,
		je,
		jne,
		jg,
		jge,
		jl,
		jle,
		sum,
		sub,
		mul,
		div,
		replace,
		call,
		ret,
		syscall,
		emplb,
		emplw,
		empld,
		emplq,
		exit,
		shl,
		smul,
		sdiv,
		shr,
		_xor,
		xcall,
		purcall,
		heapwriteb,
		heapwritew,
		heapwrited,
		heapwriteq,
		heapwriteb_vo,
		heapwritew_vo,
		heapwrited_vo,
		heapwriteq_vo,
		heapcpy,
		heapcpy_vs,
		derefw,
		derefd,
		derefq,
		derefb_vo,
		derefw_vo,
		derefd_vo,
		derefq_vo,

        // end of enum
        _last 
    };

    /*
		The following enum matches purple-core buffer names with their respective
		indexes.
	*/
    enum class buffer_address
    {
        bufA, bufB,
		bufC,
        bufE, bufD,
        cmp1, cmp2,

        // end of enum
        _last 
    };

    /*
        Returns the size in bytes of the opcode parameters.
        Knowing the size in bytes of opcode parameters is detrimental to the RTUnit, since
        it will read bytes from a bytecode file, and whenever an opcode is found, they'll read
        exactly the number of bytes returned by this function after the instruction.
    */
    int get_opcode_param_size(opcode op);

}

#endif // PURCODE_H
