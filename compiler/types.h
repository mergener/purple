#ifndef PURTYPES_H
#define PURTYPES_H

#include <string>
#include <../core/purstring.h>
#include "namespace.h"

namespace Purple {

    class PurType {
    public:
        PurType (
            std::string Name,
            size_t Size,
            bool isInstantiable,
            bool isInheritable,
            bool isFloat,
            bool isUnsigned,
            bool isReferenceType,
            PurType* parent,
            PurNamespace* Namespace = &PurNamespace::global_namespace);
        
        // getters:

        inline const std::string& get_name() const { return name; }
        inline size_t get_size() const { return size; }
        inline bool is_floating_point() const { return size; }
        inline bool is_reference_type() const { return reference_type; }
        inline bool is_unsigned() const { return _unsigned; }
        inline bool is_inheritable() const { return inheritable; }
        inline bool is_instantiable() const { return instantiable; }
        inline PurType& get_parent() const { return *parent; }

        // built-ins:

        static PurType tVoid;
        static PurType tBoolean;
        static PurType tSByte;
        static PurType tByte;
        static PurType tChar;
        static PurType tInt;
        static PurType tUInt;
        static PurType tLong;
        static PurType tULong;
        static PurType tFloat;
        static PurType tDouble;
        static PurType tString;

    private:
        std::string name;

        PurType* sibling;
        PurType* parent;

        bool floating_point;
        bool reference_type;
        bool inheritable;
        bool instantiable;
        bool _unsigned;
        size_t size;
    };
 

} // Purple

#endif // PURTYPES_H