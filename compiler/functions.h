#ifndef PUR_FUNCTIONS_H
#define PUR_FUNCTIONS_H

#include <string>
#include <vector>

#include "namespace.h"
#include "types.h"
#include "cmpobj.h"

namespace Purple {

    struct FunctionParameter 
    {
        std::string name;
        PurType* type = nullptr;
        bool is_const = false;
    };

    class PurFunction : public CompilationObject {
    public:
        PurFunction(std::string name, PurType* return_type);

        inline PurType& get_return_type() const { return *return_type; }
        inline const std::vector<FunctionParameter>& get_params() const { return params; }

    private:
        PurType* return_type;
        std::vector<FunctionParameter> params;
    };

    class PurMethod : public PurFunction {
        inline bool is_static() const { return _static; }
        inline bool is_virtual() const { return _virtual; }  

    private:
        bool _static;
        bool _virtual;
    };
} // Purple

#endif // PUR_FUNCTIONS_H