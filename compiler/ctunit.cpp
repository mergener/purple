#include "ctunit.h"
#include <sstream>

namespace Purple {
    CTUnit::CTUnit(
        const std::string& input_path,
        const std::string& output_path)
    {
        input_stream.open(input_path);
        output_stream.open(output_path);
    }
} // Purple
