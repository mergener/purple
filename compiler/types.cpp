#include "types.h"
#include "purdefs.h"

namespace Purple {

    PurType::PurType (
        std::string Name,
        size_t Size,
        bool isInstantiable,
        bool isInheritable,
        bool isFloat,
        bool isUnsigned,
        bool isReferenceType,
        PurType* parent,
        PurNamespace* Namespace)    
        : floating_point(isFloat), reference_type(isReferenceType), instantiable(isInstantiable),
            _unsigned(isUnsigned), size(Size), name(Name), inheritable(isInheritable) { }

    // built-ins:

    PurType PurType::tVoid("void", 0, false, false, false, false, false, nullptr);
    PurType PurType::tBoolean("boolean", sizeof(purbool), true, false, false, false, false, nullptr);
    PurType PurType::tSByte("sbyte", sizeof(pursbyte), true, false, false, true, false, nullptr);
    PurType PurType::tByte("byte", sizeof(purbyte), true, false, false, false, false, nullptr);
    PurType PurType::tChar("byte", sizeof(purchar), true, false, false, false, false, nullptr);
    PurType PurType::tInt("int", sizeof(purint), true, false, false, false, false, nullptr);
    PurType PurType::tUInt("uint", sizeof(puruint), true, false, false, true, false, nullptr);
    PurType PurType::tLong("long", sizeof(purlong), true, false, false, false, false, nullptr); 
    PurType PurType::tULong("ulong", sizeof(purulong), true, false, false, true, false, nullptr);
    PurType PurType::tFloat("float", sizeof(purfloat), true, false, true, false, false, nullptr);
    PurType PurType::tDouble("double", sizeof(purdouble), true, false, true, false, false, nullptr);
    PurType PurType::tString("string", sizeof(purstring), true, false, false, false, true, nullptr);

} // Purple