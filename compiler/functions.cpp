#include "functions.h"

namespace Purple {
    PurFunction::PurFunction(std::string name, PurType* return_type)
        : CompilationObject(name), return_type(return_type) {}
} // Purple
