#ifndef NAMESPACE_H
#define NAMESPACE_H

#include <unordered_map>
#include <string>

namespace Purple
{
    class PurType; // forward declaration of PurType

    class PurNamespace {
    public:
        PurNamespace(PurNamespace* parent = nullptr);

        static PurNamespace global_namespace;
        
        inline const PurNamespace& get_parent_namespace() const { return *parent; }
        const PurType& find_type_by_name(const std::string& name) const;

    private:
        std::unordered_map<std::string, PurType> types;
        PurNamespace* parent;
    };

} // Purple


#endif // NAMESPACE_H