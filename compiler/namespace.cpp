#include "namespace.h"
#include "types.h"

namespace Purple
{
    PurNamespace::PurNamespace(PurNamespace* parent)
        : parent(parent) {}

    PurNamespace PurNamespace::global_namespace;

    const PurType& PurNamespace::find_type_by_name(const std::string& name) const
    {
        const PurType* ret;
        try
        {
            ret = &types.at(name);
        }
        catch (const std::out_of_range& e)
        {
            // recursively try to find at parent namespaces
            if (parent != nullptr)
            {
                ret = &(parent->find_type_by_name(name));
            }
            else throw;
        }
        return *ret;
    }
} // Purple
