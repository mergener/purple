#ifndef CTUNIT_H
#define CTUNIT_H

#include <string>
#include <fstream>
#include <iostream>

namespace Purple {
    class CTUnit {
    public:
        CTUnit (
            const std::string& input_path,
            const std::string& output_path
        );

        void parse();
        int get_line_count() const;
        int get_column_count() const;

    private:
        std::ifstream input_stream;
        std::ofstream output_stream;

        void read_enum();
    };
} // Purple


#endif // CTUNIT_H