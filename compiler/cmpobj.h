#ifndef CMPOBJ_H
#define CMPOBJ_H

#include <string>
#include <functional>
#include "ctunit.h"

namespace Purple {   
    /*
        Compilation objects stand for everything that the code declares:
        variables, functions, classes, enums etc.
    */ 
    class CompilationObject {
    public:
        CompilationObject(std::string name);

        inline const std::string& get_name() const { return name; }
        inline int get_line_of_declaration() const { return line_of_declaration; }
        inline int get_column_of_declaration() const { return column_of_declaration; }
        inline const CTUnit& get_compilation_unit() const { return *compilation_unit; }

    private:
        std::string name;
        int line_of_declaration;
        int column_of_declaration;
        CTUnit* compilation_unit;
    };
} // Purple

#endif // CMPOBJ_H