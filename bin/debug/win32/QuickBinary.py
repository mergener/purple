#     
#   'QuickBinary.py'
#
#   Converts a file with numeric characters into an ASCII file or vice-versa.
#   The characters must be separated by spaces.
#
#   Arguments:
#       -base
#           The numeric base of the file.
#           Supported bases: 'bin', 'oct', 'dec', 'hex'.
#           Uses 'dec' by default.
#
#       -reverse
#           Converts an ASCII encoded into a numeric file.
#
#       -fo
#           Forces this program to overwrite an existing output file.
#
#   Made by Thomas Mergener
#

import os
import sys

# constants
buffersize = 50

binNumbers = "01"
octNumbers = "01234567"
decNumbers = "0123456789"
hexNumbers = "0123456789abcdefABCDEF"

# utils
class TranslationException(Exception):
    pass

def isNumber(s):
    for c in s:
        if c not in numberList:
            return False
    return True

def getThisScriptName():
    separator = ''
    
    if os.name == "nt": # Windows
        separator = '\\'
    else:
        separator = '/'
        
    return sys.argv[0][sys.argv[0].rfind(separator)+1:]

def binToNum(strSourcePath, strDestPath):
    try:
        with open(strSourcePath, "r") as src, open(strDestPath + ".temp", "w") as dest:
            buffer = src.read(buffersize)
            while buffer != '':
                for char in buffer:
                    if selectedBase == 10:
                        dest.write(str(ord(char)) + " ")

                    elif selectedBase == 2:
                        dest.write(bin(ord(char))[2:] + " ")

                    elif selectedBase == 8:
                        dest.write(oct(ord(char))[2:] + " ")

                    else:
                        dest.write(hex(ord(char))[2:] + " ")
                
                buffer = src.read(buffersize)
    except:
        # delete trash file
        if not outputFileExistedPreviously:
            os.remove(strDestPath)
        sys.exit(1)

    if outputFileExistedPreviously:
        os.remove(strDestPath)
    os.rename(strDestPath + ".temp", strDestPath)

def numToBin(strSourcePath, strDestPath):
    try:
        with open(strSourcePath, "r+") as src, open(strDestPath + ".temp", "w") as dest:
            currentLine = 1
            currentColumn = 0
            currentNumber = ""

            src.seek(0, 2)
            src.write(' ')
            src.seek(0, 0)
            
            buffer = src.read(buffersize)
            while buffer != '':
                for char in buffer:
                    currentColumn += 1
                    
                    if isNumber(char): 
                        currentNumber += char

                    elif char == " " or char == "\n" or buffer == '': # convert to binary mode
                        if currentNumber != "":
                            out = int(currentNumber, selectedBase)

                            if out < 256:
                                dest.write(chr(out))
                            else:
                                print("QuickBinary error: value ended at line %d, column %d is too high (above 255), which goes beyond the value of biggest key in the ASCII encoding." % (currentLine, currentColumn))
                                raise TranslationException()
                            
                        currentNumber = ""
                            
                        if char == "\n": 
                            currentLine += 1
                            currentColumn = 0
                            continue

                        else:  
                            continue
                    elif char == "-":
                        print("QuickBinary error: tried using '-' operator. QuickBinary only supports unsigned bytes. (line %d, column %d)" % (currentLine, currentColumn))
                        raise TranslationException()

                    else:
                        print("QuickBinary error: Unexpected character '%c' (line %d, column %d)" % (char, currentLine, currentColumn))
                        raise TranslationException()
                        
                buffer = src.read(buffersize)

            src.seek(0, 2)
            src.truncate(src.tell() - 1)
    except:
        # delete trash file
        if not outputFileExistedPreviously:
            os.remove(strDestPath)
        os.remove(strDestPath + ".temp")
        sys.exit(1)
            
    if outputFileExistedPreviously:
        os.remove(strDestPath)
    os.rename(strDestPath + ".temp", strDestPath)

# help message
def showHelpMessageAndQuit():
    print ("""
Converts an all-numbers file into a binary file with respect to the numbers already on it or vice versa.

Usage: '%s $inputfile $outputfile [args]'

Available additional arguments:

    '-base $basename' - Determines what numeric base QuickBinary should use to handle numeric files. Available bases: 'bin', 'oct', 'dec', 'hex'. Uses 'dec' by default.

    '-reverse' - If specified, instead of converting a numeric file into an ASCII file, QuickBinary will do the opposite.

    '-fo' - Overwrites the output file without extra confirmation.""" % getThisScriptName())
    sys.exit(0)

# argument parsing
def parseArgs():
    global numberList
    global reverse
    global selectedBase
    global forceOverwrite
    global outputFileExistedPreviously

    reverse = False
    forceOverwrite = False
    numberList = decNumbers
    selectedBase = 10

    if len(sys.argv) == 1:
        scriptFileName = getThisScriptName()
        print("QuickBinary error: No arguments specified. Use \"%s input_file_path output_file_path\" to parse a file or \"%s -h\" to obtain help." % (scriptFileName, scriptFileName))
        sys.exit(1)
        
    if sys.argv[1] == "-h" or sys.argv[1] == "-help" or sys.argv[1] == "/?":
        showHelpMessageAndQuit()

    if len(sys.argv) < 3:
        print("QuickBinary error: You must specify both input and output files.")
        sys.exit(1)

    inputFile = sys.argv[1]
    outputFile = sys.argv[2]
    if not os.path.isfile(inputFile):
        print("QuickBinary error: Input file \"%s\" not found." % inputFile)
        sys.exit(1)
    
    i = 3
    while i < len(sys.argv):
        arg = sys.argv[i]

        # '-base' argument        
        if arg == "-base":
            if len(sys.argv) < i + 2: # not enough parameters
                print("QuickBinary error: Missing parameter for -base argument.")
                sys.exit(1)

            baseName = sys.argv[i+1]
            
            if baseName == "dec":
                numberList = decNumbers
                selectedBase = 10
                i += 1
                
            elif baseName == "bin":
                numberList = binNumbers
                selectedBase = 2
                i += 1

            elif baseName == "oct":
                numberList = octNumbers
                selectedBase = 8
                i += 1

            elif baseName == "hex":
                numberList = hexNumbers
                selectedBase = 16
                i += 1
                
            else:
                print("QuickBinary error: Unexpected base '%s' for argument \"-base\"" % baseName)
                sys.exit(1)
                
        elif arg == "-reverse":
            reverse = True

        elif arg == "-fo":
            forceOverwrite = True
            
        else:
            print("QuickBinary error: Unknown argument %s" % arg)
            sys.exit(1)

        i += 1

    if os.path.isfile(outputFile):
        outputFileExistedPreviously = True
        if not forceOverwrite:
            confirm = input("About to overwrite existing '%s' file. Type Y (capital or not) to continue or anything else to abort: " % outputFile)
            if confirm.lower() != "y":
                print("QuickBinary: Operation cancelled.")
                sys.exit(0)
    else:
        outputFileExistedPreviously = False

# run the code
parseArgs()
if reverse:
    binToNum(sys.argv[1], sys.argv[2])
    print("QuickBinary: Succesfully converted to numeric file!")
    
else:
    numToBin(sys.argv[1], sys.argv[2])
    print("QuickBinary: Succesfully converted to ASCII file!")    
