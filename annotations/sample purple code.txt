import standard.containers.LinkedList;

namespace Game:
	enum DamageType:
		Magical, Physical, Pure;
	end

	ref class Entity:
	private:
		static LinkedList<Entity> allEntities;
	
	readonly:
		string name;
		float posX;
		float posY;	
	end

	ref class Unit extends Entity:
	private:
		static LinkedList<Unit> allUnits;
		LinkedListNode<Unit> node;

	readonly:
		float hp;
		float armor;
		float magicResistance;
		float maxHp;
		bool alive = true;

	public:
		function previewDamageOutput(float damage, DamageType type) -> float:
			switch (type):
				case DamageType.Magical:
					return damage - magicResistance;
				case DamageType.Physical:
					return damage - armor;
				case DamageType.Pure:
					return damage;
			end
		end

		function setHP(float newHP):
			if newHP > maxHP:
				hp = maxHP;

			else if newHP <= 0:
				hp = 0;
				alive = false;
			else:
				hp = newHP;
			end
		end

		constructor():
			node = allUnits.addToEnd(self);
		end

		destructor():
			allUnits.removeNode(node);
		end
	end
end