local cmd = "g++ -O3"

-- boolean flags:
local debug = true
local compilation_object = "vm" -- vm or compiler
local target_platform = "win32" -- win32, win64, linux32, linux64, macosx32, macosx64
local file_format = ".exe"

local i = 0
while i < #arg-2 do
	if arg[i] == "--release" then
		debug = false
	elseif arg[i] == "--debug" then
		debug = true

	elseif arg[i] == "-obj" then
		i = i + 1
		compilation_object = arg[i]

	elseif arg[i] == "-os" then
		i = i + 1
		target_os = arg[i]

	elseif arg[i] == "-platform" then
		i = i + 1
		target_platform = arg[i]
	end

	::continue::
	i = i + 1
end

local destination_path
if debug then
	destination_path = "bin/debug"
else
	destination_path = "bin/release"
end

destination_path = destination_path.."/"..target_platform

if compilation_object == "vm" then
	cmd = cmd.." -o "..destination_path.."/vmslow"..file_format.." purple.cpp core/purcode.cpp core/purlib.cpp core/purstring.cpp vm/internal_calls.cpp vm/memory_manager.cpp vm/rtunit.cpp vm/vm.cpp vm/vmstack.cpp"
elseif compilation_object == "compiler" then
	cmd = cmd.." -o "..destination_path.."/compiler"..file_format.." purplec.cpp core/purcode.cpp core/purlib.cpp core/purstring.cpp compiler/cmpobj.cpp compiler/ctunit.cpp compiler/functions.cpp compiler/namespace.cpp compiler/types.cpp"
else
	print("Unknown compilation object '"..compilation_object.."'. Can only build into: 'compiler' or 'vm'.")
end

print("Executing: "..cmd)
os.execute(cmd)